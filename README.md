# sewage water managment



# High Level Architechture

```plantuml
@startuml

skinparam sequenceMessageAlign center
skinparam shadowing false
skinparam sequence {
	ArrowColor #007caf
	ActorBorderColor #007caf
	LifeLineBorderColor #007caf
	LifeLineBackgroundColor #98d6e6
  ParticipantBorderColor #007caf
  ParticipantBackgroundColor #98d6e6
  BoxBorderColor #007caf
  ActorBackgroundColor #98d6e6
}

actor Operator
participant "Frontend" as F
box "Backend"
    participant "Backend API" as B
    participant "Backend Service" as S 
    participant "Database" as D
    participant "Timeseries Database" as T
endbox


group Operator requests Sewers Status
  Operator -> F: Get all the Sewers Information for the Operator
  activate Operator
  activate F
  F -> B: GET request for the Operator Sewers with their current status
  activate B
  B -> S: Forward the request to get the Sewers Meta Data of the Operator
  activate S
  S -> D: Reads the data from Database for the Operator
  activate D
  D -> S: Returns current state of all Sewers for the Operator
  destroy D
  S -> S: calcuate the Sewers that will overflow based on Rainfall + house hold flow
  S -> D: Mark operator Sewers that overflow in Alert
  S -> B: returns the Sewers data response
  destroy S
  B -> F: retruns Sewers data over Https
  destroy B
  F -> Operator: Views the Sewers State in UI
  destroy F
end

group Operator request Sewer KPIs
  Operator -> F: Get the Sewer KPIs for given X Days
  activate Operator
  activate F
  F -> B: Request the Sewer KPI over HTTPS
  activate B
  B -> S: Forwards the request to get Sewer KPIs
  activate S
  S -> T: Reads the data from Timeseries Database for X Days
  activate T
  T -> S: Returns all the Records for the Sewer over X Days
  destroy T
  S->S: extracts the required KPIs (Sewer pump run time every day (based on pump power consumption), Sewer water pumped based on Runtime, Rainfall Data over Time)
  S -> B: Returns sewer data as KPIs
  destroy S
  B -> F: Returns Sewer data over Https
  destroy B
  F -> Operator: Views the Sewer KPIs
  destroy F
end
@enduml
```
